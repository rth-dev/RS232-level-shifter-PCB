# RS232-level-shifter
a quick 'n dirty RS232 level shifter for use with 9V block battery, converting between RS232 und TTL voltage levels (bi-directional)

* Schematic Rev C - [pdf](https://gitlab.com/rth-dev/RS232-level-shifter-PCB/-/raw/master/revisions/Rev_C/RS232%20level%20shifter%20Rev_C.pdf)

![Rev C front](https://gitlab.com/rth-dev/RS232-level-shifter-PCB/-/raw/master/revisions/Rev_C/RS232%20level%20shifter%20Rev_C%20front.png)

![Rev C back](https://gitlab.com/rth-dev/RS232-level-shifter-PCB/-/raw/master/revisions/Rev_C/RS232%20level%20shifter%20Rev_C%20back.png)