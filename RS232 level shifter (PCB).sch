EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RS232 level shifter"
Date "2020-12-06"
Rev "C"
Comp "rth. engineering"
Comment1 "© 2020"
Comment2 ""
Comment3 "battery powered 9V"
Comment4 "RS232 level shifter"
$EndDescr
$Comp
L Device:CP1_Small C1
U 1 1 5986154E
P 5200 3000
F 0 "C1" H 5210 3070 50  0000 L CNN
F 1 "0.1µF" H 5210 2920 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5200 3000 50  0001 C CNN
F 3 "" H 5200 3000 50  0001 C CNN
	1    5200 3000
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C2
U 1 1 598618E4
P 5200 3450
F 0 "C2" H 5210 3520 50  0000 L CNN
F 1 "0.1µF" H 5210 3370 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5200 3450 50  0001 C CNN
F 3 "" H 5200 3450 50  0001 C CNN
	1    5200 3450
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C3
U 1 1 59861BBA
P 7250 3000
F 0 "C3" V 7150 3000 50  0000 L CNN
F 1 "0.1µF" V 7350 2900 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7250 3000 50  0001 C CNN
F 3 "" H 7250 3000 50  0001 C CNN
	1    7250 3000
	0    -1   1    0   
$EndComp
$Comp
L Device:CP1_Small C4
U 1 1 59861DFD
P 7250 3450
F 0 "C4" V 7150 3400 50  0000 L CNN
F 1 "0.1µF" V 7350 3350 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7250 3450 50  0001 C CNN
F 3 "" H 7250 3450 50  0001 C CNN
	1    7250 3450
	0    1    1    0   
$EndComp
$Comp
L Connector:DB9_Male J4
U 1 1 598621BF
P 9150 4050
F 0 "J4" H 9150 4600 50  0000 C CNN
F 1 "D-SUB 9" H 9150 3475 50  0000 C CNN
F 2 "RS232-level-shifter-PCB:DSUB-9_Free_Float_w_PCB_CutOut" H 9150 4050 50  0001 C CNN
F 3 "" H 9150 4050 50  0001 C CNN
	1    9150 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:DB9_Male J3
U 1 1 5986229D
P 3600 4050
F 0 "J3" H 3600 4600 50  0000 C CNN
F 1 "D-SUB 9" H 3600 3475 50  0000 C CNN
F 2 "RS232-level-shifter-PCB:DSUB-9_Free_Float_w_PCB_CutOut" H 3600 4050 50  0001 C CNN
F 3 "" H 3600 4050 50  0001 C CNN
	1    3600 4050
	-1   0    0    -1  
$EndComp
Text Label 5000 3800 0    60   ~ 0
TxD1_TTL
Text Label 5000 4250 0    60   ~ 0
RxD1_TTL
Text Label 7100 3800 0    60   ~ 0
TxD1_RS232
Text Label 7100 4250 0    60   ~ 0
RxD1_RS232
$Comp
L Device:CP1_Small C5
U 1 1 59866306
P 7250 2100
F 0 "C5" V 7150 2100 50  0000 L CNN
F 1 "1.0µF" V 7350 2000 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7250 2100 50  0001 C CNN
F 3 "" H 7250 2100 50  0001 C CNN
	1    7250 2100
	0    -1   1    0   
$EndComp
NoConn ~ 4050 2800
NoConn ~ 2650 2800
NoConn ~ 5550 4000
NoConn ~ 5550 4450
NoConn ~ 6950 4000
NoConn ~ 6950 4450
NoConn ~ 3900 3750
NoConn ~ 3900 3850
NoConn ~ 3900 3950
NoConn ~ 3900 4150
NoConn ~ 3900 4350
NoConn ~ 3900 4450
NoConn ~ 8850 3750
NoConn ~ 8850 3850
NoConn ~ 8850 3950
NoConn ~ 8850 4150
NoConn ~ 8850 4350
NoConn ~ 8850 4450
Text Notes 2660 7240 0    60   ~ 0
nine-volt-block
$Comp
L Device:LED D1
U 1 1 5986E79D
P 4140 6680
F 0 "D1" H 4140 6780 50  0000 C CNN
F 1 "LED" H 4140 6580 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 4140 6680 50  0001 C CNN
F 3 "" H 4140 6680 50  0001 C CNN
	1    4140 6680
	0    -1   -1   0   
$EndComp
Text Notes 3950 7240 0    60   ~ 0
LED PWR
Text Label 2350 2100 0    60   ~ 0
Vin
Text Notes 3100 4100 0    60   ~ 0
TTL
Text Notes 9450 4100 0    60   ~ 0
RS232
$Comp
L Connector_Generic:Conn_01x05 J2
U 1 1 59B44D35
P 6250 5600
F 0 "J2" V 6250 5900 50  0000 C CNN
F 1 "5pin" V 6250 5250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6250 5600 50  0001 C CNN
F 3 "~" H 6250 5600 50  0001 C CNN
	1    6250 5600
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 59B469B9
P 2720 6000
F 0 "J1" H 2720 6100 50  0000 C CNN
F 1 "Vin" H 2720 5800 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2720 6000 50  0001 C CNN
F 3 "~" H 2720 6000 50  0001 C CNN
	1    2720 6000
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 59CD6419
P 4140 6280
F 0 "R1" V 4220 6280 50  0000 C CNN
F 1 "R" V 4140 6280 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4070 6280 50  0001 C CNN
F 3 "" H 4140 6280 50  0001 C CNN
	1    4140 6280
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 59D159F6
P 4140 6930
F 0 "#PWR01" H 4140 6680 50  0001 C CNN
F 1 "GND" H 4140 6780 50  0000 C CNN
F 2 "" H 4140 6930 50  0001 C CNN
F 3 "" H 4140 6930 50  0001 C CNN
	1    4140 6930
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 59D1620C
P 8500 5260
F 0 "#PWR02" H 8500 5010 50  0001 C CNN
F 1 "GND" H 8500 5110 50  0000 C CNN
F 2 "" H 8500 5260 50  0001 C CNN
F 3 "" H 8500 5260 50  0001 C CNN
	1    8500 5260
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 59D16636
P 2350 3240
F 0 "#PWR03" H 2350 2990 50  0001 C CNN
F 1 "GND" H 2350 3090 50  0000 C CNN
F 2 "" H 2350 3240 50  0001 C CNN
F 3 "" H 2350 3240 50  0001 C CNN
	1    2350 3240
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR04
U 1 1 59D17729
P 2990 5730
F 0 "#PWR04" H 2990 5580 50  0001 C CNN
F 1 "+BATT" H 2990 5870 50  0000 C CNN
F 2 "" H 2990 5730 50  0001 C CNN
F 3 "" H 2990 5730 50  0001 C CNN
	1    2990 5730
	1    0    0    -1  
$EndComp
Text Label 4640 6000 2    60   ~ 0
Vin
Connection ~ 4140 6000
Wire Wire Line
	2920 6100 2990 6100
Connection ~ 2990 6000
Wire Wire Line
	2990 5730 2990 6000
Connection ~ 2350 3200
Wire Wire Line
	4150 3200 2350 3200
Connection ~ 8500 5200
Wire Wire Line
	2990 6100 2990 6930
Wire Wire Line
	2350 2400 2350 2500
Wire Wire Line
	4140 6830 4140 6930
Wire Wire Line
	8500 2100 8500 2700
Wire Wire Line
	6450 5200 8500 5200
Wire Wire Line
	4140 6130 4140 6000
Wire Wire Line
	4140 6430 4140 6530
Wire Wire Line
	6250 4900 6250 5400
Wire Wire Line
	6350 5050 6350 5400
Wire Wire Line
	6450 5400 6450 5200
Wire Wire Line
	6150 4900 6150 5400
Wire Wire Line
	6050 5050 6050 5400
Wire Wire Line
	4150 3650 3900 3650
Connection ~ 4300 4050
Connection ~ 8500 3450
Connection ~ 8500 3650
Wire Wire Line
	7350 3450 8500 3450
Connection ~ 8150 4050
Wire Wire Line
	8500 3650 8850 3650
Wire Wire Line
	4150 3200 4150 3650
Connection ~ 8500 2700
Wire Wire Line
	7350 2100 8500 2100
Wire Wire Line
	5200 2700 5550 2700
Wire Wire Line
	5200 1600 5200 2100
Connection ~ 5200 2100
Wire Wire Line
	4050 2100 5200 2100
Connection ~ 2350 2500
Wire Wire Line
	2650 2400 2350 2400
Wire Wire Line
	2350 2100 2650 2100
Wire Wire Line
	2650 1600 5200 1600
Wire Wire Line
	2650 2000 2650 1600
Connection ~ 2350 2700
Wire Wire Line
	2650 2700 2350 2700
Wire Wire Line
	2650 2500 2350 2500
Wire Wire Line
	8150 5050 6350 5050
Connection ~ 7950 4250
Wire Wire Line
	7950 4900 7950 4250
Wire Wire Line
	6250 4900 7950 4900
Connection ~ 4500 4250
Wire Wire Line
	4500 4900 4500 4250
Wire Wire Line
	6150 4900 4500 4900
Wire Wire Line
	4300 5050 6050 5050
Wire Wire Line
	8150 4050 8850 4050
Wire Wire Line
	8150 3800 8150 4050
Wire Wire Line
	6950 3800 8150 3800
Wire Wire Line
	6950 4250 7950 4250
Wire Wire Line
	3900 4250 4500 4250
Wire Wire Line
	4300 3800 5550 3800
Wire Wire Line
	4300 3800 4300 4050
Wire Wire Line
	4300 4050 3900 4050
Connection ~ 8500 3000
Wire Wire Line
	6950 2700 8500 2700
Wire Wire Line
	8500 3000 7350 3000
Wire Wire Line
	6950 3000 7150 3000
Wire Wire Line
	7150 3450 6950 3450
Wire Wire Line
	5550 3550 5200 3550
Wire Wire Line
	5550 3350 5200 3350
Wire Wire Line
	5550 3100 5200 3100
Wire Wire Line
	5550 2900 5200 2900
$Comp
L power:GND #PWR05
U 1 1 59D15804
P 2990 6930
F 0 "#PWR05" H 2990 6680 50  0001 C CNN
F 1 "GND" H 2990 6780 50  0000 C CNN
F 2 "" H 2990 6930 50  0001 C CNN
F 3 "" H 2990 6930 50  0001 C CNN
	1    2990 6930
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR06
U 1 1 59D1A968
P 1030 6780
F 0 "#PWR06" H 1030 6630 50  0001 C CNN
F 1 "+BATT" H 1030 6920 50  0000 C CNN
F 2 "" H 1030 6780 50  0001 C CNN
F 3 "" H 1030 6780 50  0001 C CNN
	1    1030 6780
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 59D1AD8E
P 1520 6790
F 0 "#PWR07" H 1520 6540 50  0001 C CNN
F 1 "GND" H 1520 6640 50  0000 C CNN
F 2 "" H 1520 6790 50  0001 C CNN
F 3 "" H 1520 6790 50  0001 C CNN
	1    1520 6790
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG08
U 1 1 59D1B28A
P 1520 6790
F 0 "#FLG08" H 1520 6865 50  0001 C CNN
F 1 "PWR_FLAG" H 1520 6940 50  0000 C CNN
F 2 "" H 1520 6790 50  0001 C CNN
F 3 "" H 1520 6790 50  0001 C CNN
	1    1520 6790
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG09
U 1 1 59D1B35C
P 1030 6780
F 0 "#FLG09" H 1030 6855 50  0001 C CNN
F 1 "PWR_FLAG" H 1030 6930 50  0000 C CNN
F 2 "" H 1030 6780 50  0001 C CNN
F 3 "" H 1030 6780 50  0001 C CNN
	1    1030 6780
	-1   0    0    1   
$EndComp
$Comp
L rth_voltage_regulator:MAX666 U1
U 1 1 59D1294E
P 3350 2400
F 0 "U1" H 3350 3000 50  0000 C CNN
F 1 "MAX666" H 3350 2900 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 3350 2500 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX663-MAX666.pdf" H 3350 2500 50  0001 C CNN
	1    3350 2400
	1    0    0    -1  
$EndComp
$Comp
L rth_serial_interface:UT232A U2
U 1 1 59D12BDC
P 6250 3700
F 0 "U2" H 6250 4900 60  0000 C CNN
F 1 "UT232A" H 6250 4800 60  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_LongPads" H 6250 4250 60  0001 C CNN
F 3 "http://www.unisonic.com.tw/datasheet/UT232A.pdf" H 6250 4250 60  0001 C CNN
	1    6250 3700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 59DBB07B
P 3500 6000
F 0 "SW1" H 3500 6125 50  0000 C CNN
F 1 "Switch" H 3500 5900 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3500 6000 50  0001 C CNN
F 3 "" H 3500 6000 50  0001 C CNN
	1    3500 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 6000 2990 6000
Wire Wire Line
	3700 6000 4140 6000
Wire Wire Line
	4140 6000 4640 6000
Wire Wire Line
	2990 6000 2920 6000
Wire Wire Line
	2350 3200 2350 3240
Wire Wire Line
	8500 5200 8500 5260
Wire Wire Line
	4300 4050 4300 5050
Wire Wire Line
	8500 3450 8500 3650
Wire Wire Line
	8500 3650 8500 5200
Wire Wire Line
	8150 4050 8150 5050
Wire Wire Line
	8500 2700 8500 3000
Wire Wire Line
	5200 2100 5200 2700
Wire Wire Line
	5200 2100 7150 2100
Wire Wire Line
	2350 2500 2350 2700
Wire Wire Line
	2350 2700 2350 3200
Wire Wire Line
	7950 4250 8850 4250
Wire Wire Line
	4500 4250 5550 4250
Wire Wire Line
	8500 3000 8500 3450
$EndSCHEMATC
